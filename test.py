"""
    Unit testing
"""
import unittest

class Test(unittest.TestCase):
    """
        This class has the Test Cases for the functions in utils.py
    """

    def test_firts_test_ci(self):
        a = 5
        self.assertEqual(5,a)

if __name__ == '__main__':
    unittest.main(verbosity=2)